from flask import Flask
#from flask_sqlalchemy import SQLAlchemy
from .controller.product_description import ProductDescriptionResource
from flask_restful import Api
app = Flask(__name__)

# app.debug = True

api = Api(app)
api.add_resource(ProductDescriptionResource, '/descriptions')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
#db = SQLAlchemy(app)

#db.create_all()
