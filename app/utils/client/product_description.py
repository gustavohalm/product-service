import requests


class ProductDescriptionClient():
    url = "https://servicespub.prod.api.aws.grupokabum.com.br/descricao/v1/descricao/produto/"

    def get_product_description(self, id):
        # only because request was being blocked by non-browser requests
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
        }
        res = requests.get(f"{self.url}{id}", headers=headers)
        if res.status_code == 200:
            return res.json()
        raise Exception(f'Error in Product Description Request: {res.text}')
